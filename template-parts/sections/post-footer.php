<?php 
/*----------------------------------------------------------------*\

	POST FOOTER
	Display copyright and navigation

\*----------------------------------------------------------------*/
?>

<footer class="post-footer">
	<div>
		<div class="brand-info">
			<?php dynamic_sidebar( 'footer-brand' ); ?>
		</div>
		<div class="navigations">
			<?php dynamic_sidebar( 'footer-navigation' ); ?>
			<div class="social-navigation">
				<?php dynamic_sidebar( 'footer-social' ); ?>
			</div>
		</div>
		<div class="copyright">
			<p>Copyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All rights reserved. <?php wp_nav_menu(array( 'theme_location' => 'legal_navigation' )); ?></p>
		</div>
	</div>
</footer>