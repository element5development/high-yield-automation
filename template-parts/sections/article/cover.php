<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying content within a banner

\*----------------------------------------------------------------*/
?>
<section id="section-<?php echo $template_args['sectionId']; ?>" class="cover <?php the_sub_field('width'); ?>" >
	<div>
		<?php the_sub_field('content'); ?>
	</div>
</section>
