<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying grid of cards

\*----------------------------------------------------------------*/
?>

<?php $columns = get_sub_field('columns'); ?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="card-grid <?php the_sub_field('format'); ?>-cards <?php the_sub_field('width'); ?> columns-<?php echo $columns; ?>">
	<?php if ( get_sub_field('card_grid_title') ) : ?>
		<h2><?php the_sub_field('card_grid_title'); ?></h2>
	<?php endif; ?>
	<?php while ( have_rows('cards') ) : the_row(); ?>
		<div class="card">
			<!-- IMAGE or VIDEO -->
			<?php if ( get_sub_field('video_id') ) : ?>
				<figure class="video-container">
					<iframe width="100%" height="315" src="https://www.youtube.com/embed/<?php the_sub_field('video_id') ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</figure>
			<?php elseif ( get_sub_field('image') ) : ?>
				<?php $image = get_sub_field('image'); ?>
				<figure>
					<img class="lazyload blur-up" data-expand="75" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
				</figure>
			<?php endif; ?>
			<!-- HEADLINE -->
			<?php if ( get_sub_field('title') ) : ?>
				<h3><?php the_sub_field('title') ?></h3>
			<?php endif; ?>
			<!-- DESCRIPTION -->	
			<?php if ( get_sub_field('description') ) : ?>
				<p><?php the_sub_field('description'); ?></p>
			<?php endif; ?>
			<!-- BUTTON -->
			<?php
				if ( get_sub_field('button') ) : 
					$link = get_sub_field('button'); 
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self'; 
			?>
				<a class="button is-green" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
					<?php echo esc_html($link_title); ?>
				</a>
			<?php endif; ?>
		</div>
	<?php endwhile; ?>
</section>