<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	containing a single column of selected blog posts

\*----------------------------------------------------------------*/
?>
<?php $featured_posts = get_sub_field('posts'); ?>
<?php if( $featured_posts ): ?>
	<section id="section-<?php echo $template_args['sectionId']; ?>" class="post-grid <?php the_sub_field('width'); ?>">
		<?php foreach( $featured_posts as $featured_post ): ?>
			<a href="<?php the_permalink($featured_post->ID); ?>">
				<div class="post-preview">
					<figure>
						<?php //SET FEATURED IMAGE
						if (has_post_thumbnail( $featured_post->ID ) ): 
						else :
							$image = get_field('post_default_image', 'options');
						endif; 
						?>
						<img class="lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
					</figure>
					<div class="content">
						<h3><?php echo get_the_title($featured_post->ID); ?></h3>
						<?php echo get_the_excerpt($featured_post->ID); ?>
						<span class="read-more">Read More</span>
					</div>
				</div>
			</a>
		<?php endforeach; ?>
	</section>
<?php endif; ?>