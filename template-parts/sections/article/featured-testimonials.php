<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	containing a single column of selected testimonials

\*----------------------------------------------------------------*/
?>
<?php $featured_testimonials = get_sub_field('testimonials'); ?>
<?php if( $featured_testimonials ): ?>
	<section id="section-<?php echo $template_args['sectionId']; ?>" class="testimonial-slider <?php the_sub_field('width'); ?>">
		<div>
			<?php foreach( $featured_testimonials as $featured_testimonial ): ?>
				<blockquote>
					<?php the_field('quote', $featured_testimonial->ID); ?>
					<p><?php echo get_the_title($featured_testimonial->ID); ?></p>
				</blockquote>
			<?php endforeach; ?>
		</div>
	</section>
<?php endif; ?>