<?php 
/*----------------------------------------------------------------*\

	POST HEADER
	Display the post title

\*----------------------------------------------------------------*/
?>

<header class="post-head">
	<div>
		<div class="content">
			<h1><?php the_title(); ?></h1>
			<?php if ( get_field('page_description') ) : ?>
				<?php the_field('page_description'); ?>
			<?php endif; ?>
			<?php if( get_field('page_cta') ): ?>
				<?php 
					$link = get_field('page_cta');
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
			<?php endif; ?>
		</div>
		<div class="graphic">
			<?php 
				if ( get_field('page_graphic') ) : 
					$image = get_field('page_graphic'); 
				else : 
					$image = get_field('post_default_image', 'options');
				endif;
			?>
			<img class="lazyload blur-up" data-expand="75" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
		</div>
	</div>
</header>