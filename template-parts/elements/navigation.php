<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION

\*----------------------------------------------------------------*/
?>
<nav class="primary">
	<div>
		<a href="<?php echo get_home_url(); ?>">
			<svg class="brand"><use xlink:href="#min-logo"></use></svg> 
		</a>
		<button class="menu-open">
			<svg><use xlink:href="#menu"></use></svg> 
		</button>
		<?php wp_nav_menu(array( 'theme_location' => 'primary_navigation' )); ?>
	</div>
</nav>