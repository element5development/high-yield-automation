<?php 
/*----------------------------------------------------------------*\

	DEFAULT CATEGORY ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php 
	$post_type = get_query_var('post_type'); 
	if ( $post_type == '' ) {
		$post_type = 'post';
	}
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head archive-head">
	<div class="content is-narrow">
		<h1><?php the_field($post_type.'_title','options'); ?></h1>
		<p>Currently viewing category "<?php echo single_term_title(); ?>"</p>
	</div>
</header>

<main id="main-content">
	<article>
		<section class="categories is-narrow">
			<h5>Sort by Topic</h5>
			<nav>
				<ul>
					<?php
						$categories = get_categories();
						foreach($categories as $category) {
							echo '<li><a class="button is-green is-ghost" href="' . get_category_link($category->term_id) . '">' . $category->name . '</a></li>';
						}
					?>
				</ul>
			</nav>
		</section>
		<?php if (have_posts()) : ?>
			<section class="cat-grid post-grid is-narrow">
				<?php	while ( have_posts() ) : the_post(); ?>
					<a href="<?php the_permalink($featured_post->ID); ?>">
						<div class="post-preview">
							<figure>
								<?php //SET FEATURED IMAGE
								if (has_post_thumbnail( $featured_post->ID ) ): 
								else :
									$image = get_field('post_default_image', 'options');
								endif; 
								?>
								<img class="lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
							</figure>
							<div class="content">
								<h3><?php echo get_the_title($featured_post->ID); ?></h3>
								<?php echo get_the_excerpt($featured_post->ID); ?>
								<span class="read-more">Read More</span>
							</div>
						</div>
					</a>
				<?php endwhile; ?>
			</section>
		<?php else : ?>
			<article>
				<section class="is-narrow">
					<p>Uh Oh. Something is missing. Looks like this page has no content.</p>
				</section>
			</article>
		<?php endif; ?>
	</article>
	<?php clean_pagination(); ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>