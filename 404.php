<?php 
/*----------------------------------------------------------------*\

	ERROR / NO PAGE FOUND

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head">
	<div>
		<div class="content">
			<h1>404</h1>
			<p>Happens to the best of us, this page is broken or might have been removed, hopefully the latter.</p>
			<a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">Back to Home</a>
		</div>
		<div class="graphic">
			<?php $image = get_field('post_default_image', 'options'); ?>
			<img class="lazyload blur-up" data-expand="75" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
		</div>
	</div>
</header>

<?php if( have_rows('article') ):  ?>
	<main id="main-content">
		<article>
			<?php get_template_part('template-parts/article'); ?>
			<?php if ( !empty( get_the_content() ) ) : ?>
				<section class="is-standard">
					<?php the_content(); ?>
				</section>
			<?php endif; ?>
		</article>
	</main>
<?php endif; ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>