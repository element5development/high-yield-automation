<?php
/*----------------------------------------------------------------*\
	INITIALIZE BUTTON SHORTCODE
\*----------------------------------------------------------------*/
function button_shortcode($atts, $content = null) {
	$atts = shortcode_atts(
		array(
			'color' => 'black',
			'style' => 'block',
			'target' => '_self',
			'url' => '#',
		),
		$atts,
		'button'
	);
	$style = $atts['style'];
	$color = $atts['color'];
	$target = $atts['target'];
	$url = $atts['url'];

	$link = '<a target="'.$target.'" href="'.$url.'" class="button is-'.$color.' is-'.$style.'">' . do_shortcode($content) . '</a>';
  return $link;
}
add_shortcode( 'button', 'button_shortcode' );

/*----------------------------------------------------------------*\
	VIDEO EMBED
\*----------------------------------------------------------------*/
// Create Shortcode youtube-video
// Shortcode: [youtube_video video-id=""]
function youtube_video_shortcode($atts) {
	$atts = shortcode_atts(
		array(
			'video-id' => '',
		),
		$atts,
		'youtube_video'
	);
	$videoID = $atts['video-id'];
	$iframe = '
	<div class="youtube-wrapper">
		<iframe src="https://www.youtube.com/embed/'.$videoID.'" frameborder="0" allowfullscreen></iframe>
	</div>
	';

	return $iframe;
}
add_shortcode( 'youtube_video', 'youtube_video_shortcode' );