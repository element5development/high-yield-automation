<?php
/*----------------------------------------------------------------*\
	INITIALIZE WIDGET AREA
\*----------------------------------------------------------------*/

function custom_sidebars() {
	$args = array(
		'name'          => __( 'Footer Brand', 'textdomain' ),
		'id'            => 'footer-brand',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widgettitle">',
		'after_title'   => '</h4>',
	);
	register_sidebar($args);
	$args = array(
		'name'          => __( 'Footer Navigation', 'textdomain' ),
		'id'            => 'footer-navigation',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widgettitle">',
		'after_title'   => '</h4>',
	);
	register_sidebar($args);
	$args = array(
		'name'          => __( 'Footer Social', 'textdomain' ),
		'id'            => 'footer-social',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widgettitle">',
		'after_title'   => '</h4>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'custom_sidebars' );