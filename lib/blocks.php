<?php
/*----------------------------------------------------------------*\
	DISABLE GUTENBERG
\*----------------------------------------------------------------*/
// WP < 5.0 beta
add_filter('gutenberg_can_edit_post', '__return_false', 5);
// WP >= 5.0
add_filter('use_block_editor_for_post', '__return_false', 5);
/*----------------------------------------------------------------*\
	ENABLE GUTENBERG FOR SPECIFIC POSTS BY ID
\*----------------------------------------------------------------*/
function shapeSpace_enable_gutenberg_post_ids($can_edit, $post) {
	if (empty($post->ID)) return $can_edit;
	if (428 === $post->ID || 427 === $post->ID || 426 === $post->ID || 615 === $post->ID ) return true; //cart, checkout, shop, replacement
	return $can_edit;
}
// Enable Gutenberg for WP < 5.0 beta
add_filter('gutenberg_can_edit_post', 'shapeSpace_enable_gutenberg_post_ids', 10, 2);
// Enable Gutenberg for WordPress >= 5.0
add_filter('use_block_editor_for_post', 'shapeSpace_enable_gutenberg_post_ids', 10, 2);