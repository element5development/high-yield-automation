<?php
/*----------------------------------------------------------------*\
	ENQUEUE JS AND CSS FILES
\*----------------------------------------------------------------*/
/*----------------------------------------------------------------*\
	DECLARE WOOCOMMERCE SUPPORT
\*----------------------------------------------------------------*/
function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce', array(
		'thumbnail_image_width' => 300,
		'single_image_width'    => 800,

        'product_grid'          => array(
            'default_rows'    => 3,
            'min_rows'        => 2,
            'max_rows'        => 6,
            'default_columns' => 3,
            'min_columns'     => 1,
            'max_columns'     => 3,
        ),
	) );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );
/*----------------------------------------------------------------*\
	REMOVE BREADCRUMBS
\*----------------------------------------------------------------*/
add_action( 'init', 'woo_remove_wc_breadcrumbs' );
function woo_remove_wc_breadcrumbs() {
    remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
}
/*----------------------------------------------------------------*\
	REMOVE RESULT COUNT
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
/*----------------------------------------------------------------*\
	REMOVE SPECIFIC SORT OPTIONS
\*----------------------------------------------------------------*/
add_filter( 'woocommerce_catalog_orderby', 'remove_sorting_options' );
function remove_sorting_options( $options ) {
	 unset( $options['rating'] );   
	 unset( $options['popularity'] );   
   return $options;
}
/*----------------------------------------------------------------*\
	CHANGE SALE TAG TEXT
\*----------------------------------------------------------------*/
add_filter('woocommerce_sale_flash', 'woocommerce_custom_sale_text', 10, 3);
function woocommerce_custom_sale_text($text, $post, $_product) {
	return '<span class="onsale">sale</span>';
}
/*----------------------------------------------------------------*\
	DISABLE SIDEBAR
\*----------------------------------------------------------------*/
function disable_woo_commerce_sidebar() {
	remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10); 
}
add_action('init', 'disable_woo_commerce_sidebar');
/*----------------------------------------------------------------*\
	SUPPORT GALLERY FEATURES
\*----------------------------------------------------------------*/
// add_theme_support( 'wc-product-gallery-zoom' );
// add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );
/*----------------------------------------------------------------*\
	DISABLE GALLERY IMAGE LINK
\*----------------------------------------------------------------*/
add_filter( 'woocommerce_single_product_image_thumbnail_html', 'custom_remove_product_link' );
function custom_remove_product_link( $html ) {
  return strip_tags( $html, '<div><img>' );
}
/*----------------------------------------------------------------*\
	MOVE SALE TAG ABOVE TITLE ON SINGLE PRODUCT
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_show_product_sale_flash', 2 );
/*----------------------------------------------------------------*\
	ADD PHONE BELOW PRODUCT SHORT DESCRIPTION
\*----------------------------------------------------------------*/
add_filter('woocommerce_short_description','add_text_short_descr');
function add_text_short_descr($description){
	$text= "<p>Call 1-855-449-4353 Ext. #1 to configure the communications cable lengths that you require for your system.</p>";
	return $description.$text;
}
/*----------------------------------------------------------------*\
	REMOVE PRODUCT META DATA
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
/*----------------------------------------------------------------*\
	REMOVE TABS FROM SINGLE PRODUCT
\*----------------------------------------------------------------*/
function woo_remove_product_tabs( $tabs ) {
	unset( $tabs['description'] );          // Remove the description tab
	unset( $tabs['reviews'] );          // Remove the reviews tab
	unset( $tabs['additional_information'] );   // Remove the additional information tab
	return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
/*----------------------------------------------------------------*\
	REMOVE STOCK LABEL
\*----------------------------------------------------------------*/
function my_wc_hide_in_stock_message( $html, $text, $product ) {
	$availability = $product->get_availability();
	if ( isset( $availability['class'] ) && 'in-stock' === $availability['class'] ) {
		return '';
	}
	return $html;
}
add_filter( 'woocommerce_stock_html', 'my_wc_hide_in_stock_message', 10, 3 );

/*----------------------------------------------------------------*\
	REMOVE RELATED PRODUCTS
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
/*----------------------------------------------------------------*\
	REMOVE DASHBOARD
\*----------------------------------------------------------------*/
function remove_my_account_dashboard( $menu_links ){
	unset( $menu_links['dashboard'] );
	return $menu_links;
}
add_filter( 'woocommerce_account_menu_items', 'remove_my_account_dashboard' );
function redirect_to_orders_from_dashboard(){
	if( is_account_page() && empty( WC()->query->get_current_endpoint() ) ){
		wp_safe_redirect( wc_get_account_endpoint_url( 'orders' ) );
		exit;
	}
}
add_action('template_redirect', 'redirect_to_orders_from_dashboard' );