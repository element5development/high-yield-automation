<?php 
/*----------------------------------------------------------------*\

	Template Name: Front Page

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head">
	<div>
		<div class="content">
			<h1><?php the_field('title'); ?></h1>
			<?php if ( get_field('description') ) : ?>
				<p><?php the_field('description'); ?></p>
			<?php endif; ?>
			<div>
				<?php if( get_field('button') ): ?>
					<?php 
						$link = get_field('button');
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
				<?php endif; ?>
				<?php if( get_field('button_secondary') ): ?>
					<?php 
						$link = get_field('button_secondary');
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<a class="button is-green is-ghost" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
				<?php endif; ?>
			</div>
		</div>
		<div class="graphic">
			<?php 
				if ( get_field('image') ) : 
					$image = get_field('image'); 
				else : 
					$image = get_field('post_default_image', 'options');
				endif;
			?>
			<img class="lazyload blur-up" data-expand="75" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
		</div>
	</div>
</header>

<main id="main-content">
	<article>
		<?php if ( get_field('intro') ) : ?>
			<section class="editor is-narrow">
				<?php the_field('intro'); ?>
			</section>
		<?php endif; ?>
		<?php if ( get_field('featured_product') ) : ?>
			<?php $featured_posts = get_field('featured_product'); ?>
			<section class="featured-product is-standard">
				<?php foreach( $featured_posts as $post ): ?>
					<?php $product = wc_get_product(get_the_ID()); ?>
					<div>
						<a href="<?php the_permalink(); ?>">
							<?php
								if ( has_post_thumbnail() ) :
                  $image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );
  								$image_url = $image[0];
                else :
									$image = get_field('post_default_image', 'options'); 
									$image_url = $image['sizes']['medium'];
								endif;
							?>
							<img src="<?php echo $image_url; ?>" />
						</a>
						<div>
							<a href="<?php the_permalink(); ?>">
								<h3><?php the_title(); ?></h3>
							</a>
							<div class="prices">
								<?php if ( $product->get_sale_price() ) : ?>
									<del>$<?php echo $product->get_regular_price(); ?></del>
								<?php endif; ?>
								<ins>$<?php echo $product->get_price(); ?></ins>
							</div>
							<?php echo the_excerpt(); ?>
							<a href="<?php echo get_site_url(); ?>?add-to-cart=<?php echo get_the_ID(); ?>" class="button is-green">Add to Cart</a>
						</div>
					</div>
				<?php endforeach; wp_reset_postdata(); ?>
			</section>
		<?php endif; ?>
		<?php if ( have_rows('cards') ) : ?>
			<section class="card-grid icon-cards is-standard columns-1">
				<?php while ( have_rows('cards') ) : the_row(); ?>
					<div>
						<div class="card">
							<!-- IMAGE or VIDEO -->
							<?php if ( get_sub_field('icon') ) : ?>
								<?php $image = get_sub_field('icon'); ?>
								<figure>
									<img class="lazyload blur-up" data-expand="75" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
								</figure>
							<?php endif; ?>
							<!-- HEADLINE -->
							<?php if ( get_sub_field('title') ) : ?>
								<h3><?php the_sub_field('title') ?></h3>
							<?php endif; ?>
							<!-- DESCRIPTION -->	
							<?php if ( get_sub_field('description') ) : ?>
								<?php the_sub_field('description'); ?>
							<?php endif; ?>
						</div>
					</div>
				<?php endwhile; ?>
			</section>
		<?php endif; ?>
		<?php if ( get_field('products') ) : ?>
			<?php $featured_posts = get_field('products'); ?>
			<section class="extra-products is-extra-wide">
				<h2>A solution for any size grower</h2>
				<?php foreach( $featured_posts as $post ): ?>
					<?php $product = wc_get_product(get_the_ID()); ?>
					<div>
						<a href="<?php the_permalink(); ?>">
							<?php
								if ( has_post_thumbnail() ) :
                  $image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );
  								$image_url = $image[0];
                else :
									$image = get_field('post_default_image', 'options'); 
									$image_url = $image['sizes']['medium'];
								endif;
							?>
							<img src="<?php echo $image_url; ?>" />
							<h3><?php the_title(); ?></h3>
						</a>
						<div class="prices">
							<?php if ( $product->get_sale_price() ) : ?>
								<del>$<?php echo $product->get_regular_price(); ?></del>
							<?php endif; ?>
							<ins>$<?php echo $product->get_price(); ?></ins>
						</div>
						<?php echo the_excerpt(); ?>
						<a href="<?php echo get_site_url(); ?>?add-to-cart=<?php echo get_the_ID(); ?>" class="button is-green">Add to Cart</a>
					</div>
				<?php endforeach; wp_reset_postdata(); ?>
				<div class="action-card">
					<svg><use xlink:href="#leaf"></use></svg> 
					<?php the_field('call_to_action'); ?>
				</div>
			</section>
		<?php endif; ?>


		<?php if ( have_rows('article') ) :  ?>
			<?php get_template_part('template-parts/article'); ?>
			<?php if ( !empty( get_the_content() ) ) : ?>
				<section class="is-standard">
					<?php the_content(); ?>
				</section>
			<?php endif; ?>
		<?php endif; ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>