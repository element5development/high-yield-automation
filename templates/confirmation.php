<?php 
/*----------------------------------------------------------------*\

	Template Name: Confirmation

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/post-header'); ?>

<?php if( have_rows('article') ):  ?>
	<main id="main-content">
		<article>
			<?php get_template_part('template-parts/article'); ?>
			<?php if ( !empty( get_the_content() ) ) : ?>
				<section class="is-standard">
					<?php the_content(); ?>
				</section>
			<?php endif; ?>
		</article>
	</main>
<?php endif; ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>