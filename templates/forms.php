<?php 
/*----------------------------------------------------------------*\

	Template Name: Contact Forms

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<div class="header-container editor-2-column is-extra-wide">
	<header>
		<h1><?php the_title(); ?></h1>
		<?php the_field('left_side'); ?>
	</header>
	<section>
		<?php the_field('right_side'); ?>
	</section>
</div>

<main id="main-content">
	<?php if( have_rows('article') ):  ?>
		<article>
			<?php get_template_part('template-parts/article'); ?>
			<?php if ( !empty( get_the_content() ) ) : ?>
				<section class="is-standard">
					<?php the_content(); ?>
				</section>
			<?php endif; ?>
		</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>